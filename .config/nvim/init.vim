
" Options {{{
filetype plugin indent on

set colorcolumn=88
set lazyredraw
set breakindent
set showbreak=\\\\\

set nobackup

set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4

set mouse=a

set incsearch
set smartcase
set showmatch
set ignorecase
set wildignorecase

set splitright

set updatetime=100

" set noequalalways
set equalalways
set undofile
set undodir=~/.vim/undo

let g:netrw_liststyle=3
let g:netrw_banner=0
let g:netrw_browse_split = 4
let g:netrw_winsize = 25

set fillchars+=vert:┃

set completeopt-=preview
set completeopt+=noselect
set noshowmode

set cursorline

set cino+=g0

set title
set shell=/usr/bin/mksh

" Disable some visual cruft in the preview window
autocmd WinEnter * if &previewwindow | :set colorcolumn=0 | endif

augroup filetype_txt
    autocmd!
    autocmd BufEnter,FileType *.txt :set colorcolumn=88
    autocmd BufEnter,FileType *.txt :set textwidth=88
augroup END

augroup filetype_markdown
    autocmd!
    autocmd BufEnter,FileType *.md :set colorcolumn=88
    autocmd BufEnter,FileType *.md :set textwidth=88
augroup END

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

augroup filetype_clojure
    autocmd!
    autocmd FileType clojure nnoremap <leader>r :Require<cr>
augroup END

augroup filetype_javascript
    autocmd!
    autocmd FileType javascript :set shiftwidth=2
    autocmd FileType javascript :set tabstop=2
augroup END

augroup filetype_nim
    autocmd!
    autocmd FileType nim :set shiftwidth=2
    autocmd FileType nim :set tabstop=2
augroup END

augroup filetype_lisp
    autocmd!
    autocmd FileType lisp :set shiftwidth=2
    autocmd FileType lisp :set tabstop=2
augroup END

augroup filetype_c
    autocmd!
    autocmd FileType c :setlocal colorcolumn=88
augroup END

augroup filetype_ocaml
    autocmd!
    autocmd FileType ocaml :setlocal colorcolumn=88
    autocmd FileType ocaml :setlocal commentstring=(*\ %s\ *)
augroup END

" Add merlin for ALE
let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"

" }}}

" Functions {{{

function! FileSize()
    let bytes = getfsize(expand("%:p"))
    if bytes <= 0
        return ""
    endif
    if bytes < 1024
        return bytes . "B "
    else
        return (bytes / 1024) . "K "
    endif
endfunction

" }}}

" Mappings {{{
nnoremap Q <nop>

let mapleader=','
nnoremap <leader>w :w<cr>
nnoremap <leader>q :q<cr>

nnoremap <leader>ex :Vexplore<cr>

nnoremap <leader>ev :tabedit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>:echo "reloaded init.vim"<cr>

nnoremap <tab> :tabnext<cr>
nnoremap <s-tab> :tabprev<cr>
nnoremap <c-t> :tabnew<cr>

inoremap <esc> <nop>
inoremap jk <esc>

inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>

nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>

vnoremap < <gv
vnoremap > >gv

nnoremap n nzz
nnoremap N Nzz
nnoremap <c-o> <c-o>zz
nnoremap # #zz
nnoremap * *zz

nnoremap <leader>n :set nu!<cr>
nnoremap <leader>rn :set relativenumber!<cr>

nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

nnoremap <space> za

nnoremap <leader>j :tabnext<cr>
nnoremap <leader>k :tabprev<cr>

" Select last paste
nnoremap gp `[v`]

nnoremap <leader>s :set spell!<cr>

" Delete to the black hole register
nnoremap <leader>d "_d

" Toggle search highlight
nnoremap <leader>h :noh<cr>

" Nice buffer navigation
nnoremap gb :ls<cr>:b<space>

" Show syntax highlighting groups for word under cursor
nmap <leader>p :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc
" }}}

" Plugins {{{
" Plugin list {{{
call plug#begin('~/.vim/plugged')
    Plug 'w0rp/ale'
    Plug 'airblade/vim-gitgutter'
    Plug 'mhinz/vim-startify'
    Plug 'easymotion/vim-easymotion'
    Plug 'zchee/deoplete-jedi', { 'for': 'python' }
    Plug 'https://github.com/tweekmonster/deoplete-clang2', { 'for': ['c', 'cpp'] }
    Plug 'lucy/term.vim'
    Plug 'vimlab/split-term.vim'
    Plug 'tpope/vim-commentary'
    Plug 'https://github.com/tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'majutsushi/tagbar'
    Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'}
    Plug 'junegunn/fzf.vim'
    Plug 'https://github.com/sheerun/vim-polyglot'
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    Plug 'https://github.com/Shougo/neosnippet.vim'
    Plug 'https://github.com/Shougo/neosnippet-snippets'
    Plug 'https://github.com/junegunn/goyo.vim'
    Plug 'https://github.com/junegunn/limelight.vim'
    Plug 'https://github.com/morhetz/gruvbox'
    Plug 'https://github.com/eraserhd/parinfer-rust'
    Plug 'https://github.com/Yggdroot/indentLine'
    Plug 'https://github.com/jremmen/vim-ripgrep'
    " COMMON LISP
    Plug 'l04m33/vlime', {'rtp': 'vim/'}
    " GO
    Plug 'https://github.com/fatih/vim-go', {'for': 'go'}
    Plug 'https://github.com/zchee/deoplete-go', { 'do': 'make', 'for': 'go'}
call plug#end()
" }}}

" Vim-ripgrep {{{
nnoremap <leader>rg :Rg<cr>
" }}}

" Goyo {{{
nnoremap <leader>g :Goyo 88<cr>
" }}}

" Limelight {{{
nnoremap <leader>ll :Limelight!!<cr>
" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#928374'

let g:limelight_default_coefficient = 0.5
" }}}

" Neosnippets {{{
let g:neosnippet#enable_completed_snippet = 1
" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
" \ pumvisible() ? "\<C-n>" :
" \ neosnippet#expandable_or_jumpable() ?
" \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=nv
endif
" }}}

" Deoplete {{{
let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#jedi#python_path = "/usr/bin/python3"
let g:deoplete#sources#go#gocode_binary = "/home/bilebucket/go/bin/gocode"
let g:deoplete#sources#go#source_importer = 1
call deoplete#custom#option({
            \ 'ignore_case': v:false,
            \ })
let g:deoplete#sources#clang#std = { 'c': 'c99' }
" }}}

" Tagbar {{{
nnoremap <leader>t :TagbarToggle<cr>
let g:tagbar_left = 1
let g:tagbar_autoclose = 0
let g:tagbar_autofocus = 1
let g:tagbar_compact = 1
let g:tagbar_autopreview = 0
let g:tagbar_previewwin_pos = 'topleft'

let g:tagbar_type_d = {
            \ 'ctagstype' : 'd',
            \ 'kinds'     : [
            \ 'c:classes:1:1',
            \ 'f:functions:1:1',
            \ 'T:template:1:1',
            \ 'g:enums:1:1',
            \ 'e:enumerators:0:0',
            \ 'u:unions:1:1',
            \ 's:structs:1:1',
            \ 'v:variables:1:0',
            \ 'i:interfaces:1:1',
            \ 'm:members',
            \ 'a:alias'
            \ ],
            \'sro': '.',
            \ 'kind2scope' : {
            \ 'c' : 'class',
            \ 'g' : 'enum',
            \ 's' : 'struct',
            \ 'u' : 'union',
            \ 'T' : 'template'
            \},
            \ 'scope2kind' : {
            \ 'enum'      : 'g',
            \ 'class'     : 'c',
            \ 'struct'    : 's',
            \ 'union'     : 'u',
            \ 'template'  : 'T'
            \ },
            \ 'ctagsbin' : 'dscanner',
            \ 'ctagsargs' : ['--ctags']
            \ }
" }}}

" GitGutter {{{
let g:gitgutter_grep=''
let g:gitgutter_terminal_reports_focus=0
nnoremap <leader>gg :GitGutterBufferToggle<cr>
" }}}

" ALE {{{

let g:ale_sign_error = '⌁'
let g:ale_sign_warning = '⌁'

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_fixers = {
            \'*': ['remove_trailing_lines', 'trim_whitespace'],
            \'c': ['uncrustify'],
            \'cpp': ['uncrustify'],
            \'d': ['uncrustify'],
            \'haskell': ['hlint'],
            \'javascript': ['prettier', 'eslint'],
            \'python': ['black'],
            \'rust': ['rustfmt'],
            \}

let g:ale_linters = {
            \'python': ['flake8', 'pylint'],
            \}

let g:ale_fix_on_save = 1
let g:ale_lint_on_text_changed = 'normal'
let g:ale_sh_shellcheck_options = '-s ksh'

let g:ale_c_gcc_executable = "/usr/bin/clang"
let g:ale_c_gcc_options = "-std=c99 -Wall -Werror -Wpedantic -Weverything"

nnoremap <leader>an :ALENextWrap<cr>zz
nnoremap <leader>ap :ALEPreviousWrap<cr>zz
nnoremap <leader>at :ALEToggle<cr>
nnoremap <leader>af :ALEFix<cr>
nnoremap <leader>ad :ALEDetail<cr>
" }}}

" IndentLine {{{
let g:indentLine_char = '┊'
let g:indentLine_enabled = 0
nnoremap <leader>i :IndentLinesToggle<cr>
" }}}

" FZF {{{

nnoremap <leader>ff :FZF<cr>
nnoremap <leader>fb :Buffers<cr>
nnoremap <leader>fl :BLines<cr>
nnoremap <leader>fll :Lines<cr>
nnoremap <leader>fh :History<cr>

" }}}

" }}}

" Colorscheme {{{
set background=dark
set termguicolors

let g:gruvbox_contrast_light='medium'
let g:gruvbox_italic=1
let g:gruvbox_bold=1
colorscheme gruvbox

" For transparent/terminal background
hi Normal guibg=None ctermbg=None
hi StatuslineNC gui=reverse,italic

" }}}

" Statusline {{{

function! StatuslineColor()
    if mode() == "i"
        return ''
    endif

    let l:counts = ale#statusline#Count(bufnr(''))
    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors

    if (l:all_errors > 0)
        exe 'hi! Statusline guibg=#fb4934 guifg=#282828 gui=none'
    " elseif (l:all_non_errors > 0)
    "     exe 'hi! Statusline guifg=yellow'
    else
        exe 'hi! Statusline guibg=#98971a guifg=#282828 gui=none'
    endif

    return ''
endfunc

" inspired by https://github.com/junegunn/dotfiles/blob/master/vimrc#L208
function! s:statusline_expr()
    let l:mo = "%{&modified ? '[+] ' : !&modifiable ? '[-] ' : ''}"
    let l:ft = "%{len(&filetype) ? '['.&filetype.'] ' : ''}"
    let l:ro = "%{&readonly ? ' ' : ''}"
    let l:errors = ale#statusline#Count(bufnr('')).error
    let l:ls = "%{ale#statusline#Count(bufnr('')).error == 0 ? '✔' : '✘ '. ale#statusline#Count(bufnr('')).error . 'E'}"
    let l:fs = "%{FileSize()}"
    let l:fug = "%{FugitiveStatusline()} "

    let l:sep = "%="

    return '%{StatuslineColor()} %f %<'.l:mo.l:ro.l:ft.l:fs.l:ls.l:sep.l:fug.'[%c,%l/%L] %p%% '
endfunc

let &statusline = s:statusline_expr()
" }}}
